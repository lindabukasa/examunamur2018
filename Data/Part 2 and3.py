import random
from random import randint
from random import choice
import pandas as pd
import numpy as np

# PARTIE 2 ET 3
path = '..//Data/'
Proba_Food = pd.read_csv(path + 'Food_Probabilities2.csv',
                         sep=';')
Proba_Drink = pd.read_csv(path + 'Drink_Probabilities2.csv',
                          sep=';')  # import the probabilities we will use for the simulation into a data frame
print(Proba_Food)
print(Proba_Drink)


class Client(object):  # 1st "big" class, with all the customers

    def __init__(self, Client_ID, cash, Returning_Clients):
        self.cash = cash
        self.previousFood = []
        self.previousDrink = []
        self.ID = Client_ID
        self.Returning_Clients = Returning_Clients

    # Create a function that can calculate how much money they still have

    def purchase(self, Total_Price, Type_Food, Type_Drink):
        self.cash = self.cash - Total_Price
        self.previousDrink.append(Type_Drink)

        if (Type_Food != 'NaN'):
            self.previousFood.append(Type_Food)  # if it is not NaN, we add it to 'Type_Food'

    def Tip(self):
        t = randint(0, 10)
        self.cash -= t  # correct the error
        return t

    # Create a subclass Client Tripadvisor (heritance) because those clients don't have the same behavior as the other (tip)


class Client_Tripadvisor(Client):

    def __init__(self, Client_ID, cash):
        Client.__init__(cash, Client_ID, True)

    def Tip(self):
        t = randint(0, 10)
        self.cash -= t  # it will take count of the tip when it calculates the amount of money left
        return t


# create the 2nd big class
class Coffee_Bar(object):
    def __init__(self, Price_Drink, Price_Food, Probabilities_Food,
                 Probabilities_Drink):  # Probabilities are added later with the simulation
        self.Price_Drink = Price_Drink
        self.Price_Food = Price_Food
        self.memory = []
        self.cash = 0
        self.Probabilities_Drink = Probabilities_Drink
        self.Probabilities_Food = Probabilities_Food

    # Function with the different prices of food
    def Prices_Food(self, Food_Type):
        if (Food_Type == 'NaN'):
            return 0  # if the client does not order any food(NaN), Python will return 0€
        if (Food_Type == 'sandwich'):
            return 5
        if (Food_Type == 'cookie'):
            return 2
        if (Food_Type == 'pie'):
            return 3
        if (Food_Type == 'muffin'):
            return 3

    # Function with the prices of the different drinks
    def Prices_Drink(self, Drink_Type):
        if (Drink_Type == 'coffee'):
            return 3
        if (Drink_Type == 'water'):
            return 2
        if (Drink_Type == 'frappucino'):
            return 4
        if (Drink_Type == 'tea'):
            return 3
        if (Drink_Type == 'milkshake'):
            return 5
        if (Drink_Type == 'soda'):
            return 3

    # Function - choose randomly a drink following the different probabilities
    def Drink_Purchase(self, hour):
        Proba = self.Probabilities_Drink[self.Probabilities_Drink['time'] == hour]
        choice = randint(0, 100)  # choose randomly a probability between 0 and 100%

        Drinks_list = list(Proba.columns)  # create a list with the probabilities of the different drinks
        Drinks_list.pop(0)

        probabilities = np.delete(Proba.values[0], 0)  # numpy for getting the list
        Probabilities_list = []

        for p in probabilities:
            Probabilities_list.append(p)

        index = np.random.choice(len(Drinks_list), 1, p=Probabilities_list)  # numpy for getting the list

        return Drinks_list[index[0]]  # return the list with an index

    # Function - choose randomly a kind of food or not following the different probabilities (same as the drinks)
    def Food_Purchase(self, hour):
        Proba = self.Probabilities_Food[self.Probabilities_Food['time'] == hour]
        choice = randint(0, 100)

        Food_list = list(Proba.columns)
        Food_list.pop(0)

        probabilities = np.delete(Proba.values[0], 0)

        Probabilities_list = []
        for p in probabilities:
            Probabilities_list.append(p)

        index = np.random.choice(len(Food_list), 1, p=Probabilities_list)

        return Food_list[index[0]]

    # Function - When the client comes, what he orders and how much he pays
    def Client_Arrival(self, hour, Customers):  # Customers added later

        food = self.Food_Purchase(hour)
        drink = self.Drink_Purchase(hour)

        Food_Price = self.Prices_Food(food)
        Drink_Price = self.Prices_Drink(drink)

        Client.purchase(Food_Price + Drink_Price, food, drink)
        self.cash += (Food_Price + Drink_Price)

        if (isinstance(Client, Client_Tripadvisor)):  # mark the "exception", tripadvisor clients give a tip
            self.cash += Client.Tip

        self.memory.append([Client.ID, hour, food, drink])

    def Highest_Order(self):
        return 10  # price sandwich = 5, price milkshake = 5


#Begin the simulation
class Final_Simulation:
    def __init__(self):
        self.Returning_clients = []
        self.Clients = []
        self.StartID = 147258

# 1000 Returning customers
Clients = []
Returning_Clients = []
StartID = 147258

for a in range(0, 1000):
    choice = randint(1, 3) #1/3 of hipsters and 2/3 of regular
    StartID += 1

    if choice == 1: # if it is equal to 1 it is a hipster
        A = Client(500, ('CID' + str(StartID)), True)
        Returning_Clients.append(A)

    else: #if it is equal to 2 or 3, it is a regular
        A = Client(250, ('CID' + str(StartID)), True)
        Returning_Clients.append(A)

print(len(Returning_Clients))


#One time customers

def Client_Coming(Returning_Clients, StartID):
    probabilities = randint(1, 100)

    if (probabilities <= 20):
        return random.choice(Returning_Clients)
    else:
        probabilities = randint(1, 100)
        StartID += 1

        if (probabilities == 10):
            return Client_Tripadvisor(100, ('CID' + str(StartID)))

        else:
            return Client(100, ('CID' + str(StartID)), False)


# Prix des boissons et nourriture
Price_Food = {'sandwich': 5, 'muffin': 3, 'pie': 3, 'cookie': 2, ' ': 0}
Price_Drink = {'milkshake': 5, 'frappucino': 4, 'soda': 3, 'coffee': 3, 'tea': 3, 'water': 2}

coffee_bar = Coffee_Bar(Price_Food, Proba_Food, Price_Drink, Proba_Drink)

# Food
for row in Proba_Food.iterrows():
    hour = row[1].time

    Customers = Client_Coming(Returning_Clients, StartID)
    StartID += 1
    coffee_bar.Client_Arrival(Customers, hour)

    # Gestion des clients, si il a plus d'argent, on le vire

    if (Customers.Returning_Clients):
        if (Customers.cash < Coffee_Bar.Highest_Order):
            print('Goodbye')

    else:
        Clients.append(Customers)

print(coffee_bar.memory)
